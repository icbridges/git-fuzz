import os
import copy
import pathlib
import pygit2
import configparser
import subprocess

# Used for performing sanity checks on config files
from supported_configs import *

GITFUZZ_DIR = "git-fuzz"
GITFUZZ_CONFIG = "git-fuzz_config.ini"

class GitFuzz():

    def __init__(self):
        """
        """
        self.gitfuzz_dir = "git-fuzz"
        self.gitfuzz_config = "git-fuzz_config.ini"
        self.config = configparser.ConfigParser()

    def process_config(self):
        """
        """
        print("INFO: processing git-fuzz config")

        # Make sure the config exists
        if not self._config_file_exists():
            return False

        # Parse the config file
        self._parse_config()

        # Perform sanity checks
        if not self._config_check():
            return False

    def _config_file_exists(self):
        """
        """
        print("INFO: checking for git-fuzz directory")
        if not os.path.isdir(GITFUZZ_DIR):
            print("ERROR: could not find git-fuzz directory")
            return False

        print("INFO: checking for git-fuzz config file")
        if not os.path.isfile(str(pathlib.PurePath(GITFUZZ_DIR, 
                                                   GITFUZZ_CONFIG))):
            print("ERROR: could not find git-fuzz_config file")
            return False

        return True

    #TODO: This entire process needs to be cleaned up. More strict definitions
    # of "required" and "optional" need to be applied
    def _check_config_section(self, config, section, required = False):
        """
        """
        # Make sure the field exists if it is required
        if required:
            if section not in self.config:
                print("ERROR: Config missing required section: %s" % (section))
                return False
        # If an optional field is missing, skip it
        else:
            if section not in self.config:
                return True
            
        # Make sure all of the required entries in the section exist
        for entry in config[section]:
            if entry not in self.config[section]:
                print("ERROR: Config section %s missing entry %s" % 
                                                (section, entry))
                return False
            
            # Make sure the entry contains a support value (if applicable)
            if self.config[section][entry] not in config[section][entry]:
                print("ERROR: %s is not a valid value for %s" % (
                                self.config[section][entry], entry))
                return False
        
        return True

    def _config_check(self):
        """
        """
        sane = True

        print("INFO: sanity checking user-supplied git-fuzz config file")
        
        # Perform sanity checks on all of the required fields
        for section in BASE_CONFIG_SUPPORT:
            sane = self._check_config_section(BASE_CONFIG_SUPPORT, 
                                              section, 
                                              True)
            if not sane:
                return False      

        # Perform sanity checks on all of the project-specific fields
        for section in PROJECT_CONFIG_SUPPORT:
            sane = self._check_config_section(PROJECT_CONFIG_SUPPORT, 
                                              section)
            if not sane:
                return False
        
        return sane
    
    def _parse_config(self):
        """
        """
        print("INFO: parsing git-fuzz config file")
    
        # Parse the config file
        self.config.read(str(pathlib.PurePath(GITFUZZ_DIR, GITFUZZ_CONFIG)))

    def build(self):
        """
        """
        built = False
        if self.config["BUILDING"]["Build"] != "True":
            print("INFO: project not configured for building")
            return True

        print("INFO: project building required")

        build_tool = self.config["BUILDING"]["BUILDTOOL"]
        if build_tool == "Make":
            built = self._make_build()
        else:
            print("ERROR: git-fuzz does not support %s", build_tool)
            built = False

        return built

    # TODO: I expected a Python library that builds Makefiles to already exist.
    # However, initial research only yielded a bunch of deprecated projects.
    # Additional searching should be done to find a cleaner solution.
    def _make_build(self):
        """
        """
        # Locate the Makefile via the git-fuzz config
        make_path = str(pathlib.PurePath(self.config["MAKE"]["Path"]))
        print(make_path) 
        if not os.path.isfile(make_path):
            print("ERROR: could not locate Makefile")
            return False 

        # Save current directory
        cur_path = os.getcwd()

        # Change CWD to Makefile directory
        os.chdir(os.path.dirname(make_path))

        # TODO: Run the make command, harvest the output
        try:
            subprocess.check_call(["make"]) 
        except subprocess.CalledProcessError:
            print("ERROR: make failed")
            return False

        # Restore the previous CWD
        os.chdir(cur_path)
        
        return True

if __name__ == "__main__":
    fuzzer = GitFuzz()
    fuzzer.process_config()
    fuzzer.build()
