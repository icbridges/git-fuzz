# Base, required fields
BASE_CONFIG_SUPPORT = {
    "FUZZING":{
        "Fuzzer":["AFL"],  
    },
    "BUILDING":{
        "Build":["True", "False"],
        "BuildTool":["Make"]
   },
}

# Project-specific fields
PROJECT_CONFIG_SUPPORT = {
    "AFL":{
        "Mode":["qemu"],
    },
    "MAKE":{},
}
